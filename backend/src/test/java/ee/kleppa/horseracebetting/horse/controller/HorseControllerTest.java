package ee.kleppa.horseracebetting.horse.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.kleppa.horseracebetting.horse.domain.Horse;
import ee.kleppa.horseracebetting.horse.repository.HorseRepository;
import ee.kleppa.horseracebetting.horse.service.HorseService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class HorseControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private HorseRepository horseRepository;

	@Autowired
	private HorseService horseService;


	@Test
	void saveHorse() throws Exception {
		Horse horse = Horse.builder().name("Zara").color("Palomino").build();
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/horse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(horse)))
				.andExpect(status().isOk())
				.andReturn();
		Horse savedHorse = horseRepository.findById(objectMapper.readValue(result.getResponse().getContentAsString(),
				Horse.class).getId()).orElseThrow();
		assertEquals("Zara", savedHorse.getName());
	}

	@Test
	void saveHorseOptionals() throws Exception {
		Horse horse = Horse.builder().name("Zara").color("Palomino").age(4).quality(10).weight(60).build();
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/horse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(horse)))
				.andExpect(status().isOk())
				.andReturn();
		Horse savedHorse = horseRepository.findById(objectMapper.readValue(result.getResponse().getContentAsString(), Horse.class).getId()).orElseThrow();
		assertEquals(10, savedHorse.getQuality());
		assertEquals(60, savedHorse.getWeight());
		assertEquals(4, savedHorse.getAge());
	}

	@Test
	void getAllHorses() throws Exception {
		Horse horse1 = Horse.builder().name("Zara").color("Palomino").build();
		Horse horse2 = Horse.builder().name("Bella").color("Black").build();
		horseService.saveNewHorse(horse1);
		horseService.saveNewHorse(horse2);
		mockMvc.perform(MockMvcRequestBuilders.get("/horse")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()", Matchers.greaterThanOrEqualTo(2)));
	}

	@Test
	void getHorseById() throws Exception {
		Horse horse = Horse.builder().name("Zara").color("Palomino").build();
		horseService.saveNewHorse(horse);
		mockMvc.perform(MockMvcRequestBuilders.get("/horse/id/{id}", 1)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(result -> assertEquals("Zara",
						objectMapper.readValue(result.getResponse().getContentAsString(), Horse.class).getName()));
	}

}
