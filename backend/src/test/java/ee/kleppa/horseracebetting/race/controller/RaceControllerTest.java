package ee.kleppa.horseracebetting.race.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.kleppa.horseracebetting.horse.domain.Horse;
import ee.kleppa.horseracebetting.horse.repository.HorseRepository;
import ee.kleppa.horseracebetting.horse.service.HorseService;
import ee.kleppa.horseracebetting.race.domain.Race;
import ee.kleppa.horseracebetting.race.repository.RaceRepository;
import ee.kleppa.horseracebetting.race.service.RaceService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RaceControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private HorseRepository horseRepository;
	@Autowired
	private HorseService horseService;

	@Autowired
	private RaceRepository raceRepository;
	@Autowired
	private RaceService raceService;

	@Test
	void saveRace() throws Exception {
		Horse horse1 = Horse.builder().name("Zara").color("Palomino").build();
		Horse horse2 = Horse.builder().name("Bella").color("Black").build();
		horse1 = horseService.saveNewHorse(horse1);
		horse2 = horseService.saveNewHorse(horse2);
		List<Horse> horsesList = new ArrayList<>();
		System.out.println(horsesList.add(horse1));
		System.out.println(horsesList.add(horse2));
		Race race = Race.builder().raceDate(new Date()).track("Ascot")
				.participatingHorses(horsesList).bettedHorse(horse1).build();
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/race")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(race)))
				.andExpect(status().isOk())
				.andReturn();
		Race savedRace = raceRepository.findById(objectMapper.readValue(result.getResponse().getContentAsString(), Race.class).getId()).orElseThrow();
		assertEquals(horse1.getName(), savedRace.getBettedHorse().getName());
		assertEquals(1, savedRace.getWinningHorse().getWins());
	}

	@Test
	void getAllRaces() throws Exception {
		Horse horse1 = Horse.builder().name("Zara").color("Palomino").build();
		Horse horse2 = Horse.builder().name("Bella").color("Black").build();
		horse1 = horseService.saveNewHorse(horse1);
		horse2 = horseService.saveNewHorse(horse2);
		List<Horse> horsesList = new ArrayList<>();
		horsesList.add(horse1);
		horsesList.add(horse2);
		Race race1 = Race.builder().raceDate(new Date()).track("Ascot")
				.participatingHorses(horsesList).bettedHorse(horse1).build();
		Race race2 = Race.builder().raceDate(new Date()).track("Saratoga")
				.participatingHorses(horsesList).bettedHorse(horse1).build();
		raceService.saveNewRace(race1);
		raceService.saveNewRace(race2);
		mockMvc.perform(MockMvcRequestBuilders.get("/race")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()", Matchers.greaterThanOrEqualTo(2)));
	}

}
