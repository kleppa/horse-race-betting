package ee.kleppa.horseracebetting.race.service;

import ee.kleppa.horseracebetting.horse.domain.Horse;
import ee.kleppa.horseracebetting.horse.service.HorseService;
import ee.kleppa.horseracebetting.race.domain.Race;
import ee.kleppa.horseracebetting.race.repository.RaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
@Service
public class RaceService {
	private final RaceRepository raceRepository;
	private final HorseService horseService;

	public Race saveNewRace(Race race) {
		double bestResult = 0;
		Horse winningHorse = race.getParticipatingHorses().get(0);
		for (Horse horse : race.getParticipatingHorses()) {
			double ageModifier = 1;
			if (horse.getAge() < 4) {
				ageModifier = ageModifier - (4 - horse.getAge()) * 0.05;
			} else if (horse.getAge() > 5) {
				ageModifier = ageModifier - (horse.getAge() - 5) * 0.05;
			}
			double qualityModifier = 1;
			if (horse.getQuality() < 5) {
				qualityModifier = qualityModifier - (5 - horse.getQuality()) * 0.1;
			} else if (horse.getQuality() > 5) {
				qualityModifier = qualityModifier + (horse.getQuality() - 5) * 0.1;
			}
			double horseResult = (getRandomNumber(1, 10) - (horse.getWeight() - 60)*0.1) * ageModifier * qualityModifier;
			if (bestResult < horseResult) {
				bestResult = horseResult;
				winningHorse = horse;
			}
			System.out.println(horseResult);
		}
		race.setWinningHorse(horseService.addWin(winningHorse));
		return raceRepository.save(race);
	}

	public List<Race> getAllRaces() {
		return raceRepository.findAll();
	}

	public int getRandomNumber(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}
}
