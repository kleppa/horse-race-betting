package ee.kleppa.horseracebetting.race.repository;

import ee.kleppa.horseracebetting.race.domain.Race;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaceRepository extends JpaRepository<Race, Long> {
}
