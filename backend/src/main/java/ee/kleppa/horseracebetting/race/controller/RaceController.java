package ee.kleppa.horseracebetting.race.controller;

import ee.kleppa.horseracebetting.race.domain.Race;
import ee.kleppa.horseracebetting.race.service.RaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/race")
@RequiredArgsConstructor
public class RaceController {
	private final RaceService raceService;

	@GetMapping
	public List<Race> getAllRaces() {
		return raceService.getAllRaces();
	}

	@PostMapping()
	public Race saveRace(@RequestBody Race race) {
		return raceService.saveNewRace(race);
	}

}
