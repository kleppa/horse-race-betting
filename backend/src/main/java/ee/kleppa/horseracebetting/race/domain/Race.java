package ee.kleppa.horseracebetting.race.domain;

import ee.kleppa.horseracebetting.horse.domain.Horse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name="race")
public class Race {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String track;
	private Date raceDate;

	@ManyToMany
	@JoinColumn(name = "pariticipating_horse_id")
	private List<Horse> participatingHorses;

	@ManyToOne
	@JoinColumn(name = "betted_horse_id")
	private Horse bettedHorse;

	@ManyToOne
	@JoinColumn(name = "winning_horse_id")
	private Horse winningHorse;
}
