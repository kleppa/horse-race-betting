package ee.kleppa.horseracebetting.horse.controller;

import ee.kleppa.horseracebetting.horse.domain.Horse;
import ee.kleppa.horseracebetting.horse.service.HorseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/horse")
@RequiredArgsConstructor
public class HorseController {


	private final HorseService horseService;

	@GetMapping
	public List<Horse> getAllHorses() {
		return horseService.getAllHorses();
	}

	@GetMapping("/id/{id}")
	public Horse getHorseById(@PathVariable Long id) {
		return horseService.getHorseById(id);
	}

	@PostMapping()
	public Horse saveHorse(@RequestBody Horse horse) {
		return horseService.saveNewHorse(horse);
	}
}
