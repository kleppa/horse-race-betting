package ee.kleppa.horseracebetting.horse.service;

import ee.kleppa.horseracebetting.horse.domain.Horse;
import ee.kleppa.horseracebetting.horse.repository.HorseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
@Service
public class HorseService {

	private final HorseRepository horseRepository;

	public List<Horse> getAllHorses() {
		return horseRepository.findAll();
	}

	public Horse getHorseById(Long id) {
		return horseRepository.findById(id).orElseThrow(EntityNotFoundException::new);
	}

	public Horse saveNewHorse(Horse horse) {
		if (horse.getAge() == null) {
			horse.setAge(getRandomNumber(2, 15));
		}
		if (horse.getWeight() == null) {
			horse.setWeight(getRandomNumber(60, 90));
		}
		if (horse.getQuality() == null) {
			horse.setQuality(getRandomNumber(1, 10));
		}
		horse.setWins(0);
		return horseRepository.save(horse);
	}

	public Integer getRandomNumber(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}

	public Horse addWin(Horse horse) {
		horse.setWins(horse.getWins()+1);
		return horseRepository.save(horse);
	}
}
