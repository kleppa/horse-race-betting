package ee.kleppa.horseracebetting.horse.repository;

import ee.kleppa.horseracebetting.horse.domain.Horse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HorseRepository extends JpaRepository<Horse, Long> {
}
