package ee.kleppa.horseracebetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HorseRaceBettingApplication {

	public static void main(String[] args) {
		SpringApplication.run(HorseRaceBettingApplication.class, args);
	}

}
