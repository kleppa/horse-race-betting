# horse-race-betting


## Frontend

* React
* Typescript
* MUI

## Backend

* Spring Boot

## Deploy

* AWS EC2 server
* [Link](http://13.53.172.224)

## Extra info

* To add a horse you need give it a name and choose a color for it,
the rest are optional and will be randomized if left empty.
* A horses race results are based on generating a random number between one and ten and modifying it
with weight, age and quality modifiers. Highest result wins.
* A Horses weight can range from 60-90. Higher weight means higher negative modifier.
* A Horses age can range from 2-15. 4-5 is peak age for a race horse.
The more the age deviates from that range the higher the negative modifier.
* A Horses quality can range from 1-10. 5 is avarage, higher than that means a positive modifier,
lower means a negative modifier.



