import {Horse} from "./HorseModel";

export interface Race {
    id?: number,
    track: string
    raceDate: Date,
    participatingHorses: Horse[],
    bettedHorse: Horse,
    winningHorse?: Horse,
}
