export interface Horse {
    id?: number,
    name: string
    color?: string,
    weight: number | undefined,
    age: number | undefined,
    quality: number | undefined,
    wins?: number
}
