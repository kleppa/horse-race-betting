import React from 'react';
import {Grid} from "@mui/material";
import {RaceCreation} from "./RaceCreation";

export const HomePageView = () => {
    return (
        <Grid container justifyContent="center" spacing={3} mt={2}>
            <Grid item xs={6}>
                <RaceCreation/>
            </Grid>
        </Grid>
    )
}
