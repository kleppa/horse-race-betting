import React, {useEffect, useState} from 'react';
import {
    Box,
    Button, CircularProgress,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Step,
    StepLabel,
    Stepper,
    TextField,
    Typography
} from "@mui/material";
import {DataGrid} from '@mui/x-data-grid';
import {HorseAPI} from "../api/HorseAPI";
import {Horse} from "../model/HorseModel";
import {RaceAPI} from "../api/RaceAPI";
import {Race} from "../model/RaceModel";
import {AdapterMoment} from '@mui/x-date-pickers/AdapterMoment';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {DateTimePicker} from '@mui/x-date-pickers/DateTimePicker';
import {horseColumns} from "../utils/DataGridColumnDef";


const steps = ['Set up race', 'Bet on a horse', 'Results'];
const raceTracks = [
    {name: "Churchill Downs"},
    {name: "Ascot"},
    {name: "Longchamp"},
    {name: "Meydan"},
    {name: "Aintree"},
    {name: "Saratoga"},
    {name: "Tokyo"},
    {name: "Flemington"},
    {name: "Epsom Downs"},
    {name: "Santa Anita Park"}
]

export const RaceCreation = () => {
    const [activeStep, setActiveStep] = useState(0);
    const [skipped, setSkipped] = useState(new Set<number>());
    const [dateTime, setDateTime] = useState<Date>(new Date());
    const [track, setTrack] = useState<string>('');
    const [horses, setHorses] = useState<Horse[]>([]);
    const [isFetching, setIsFetching] = useState(true);
    const [checked, setChecked] = useState<Horse[]>([]);
    const [bettedHorse, setBettedHorse] = useState<Horse | undefined>();
    const [raceResults, setRaceResults] = useState<Race | undefined>();

    const handleNext = () => {
        let newSkipped = skipped;
        if (activeStep === 1) {
            const race: Race = {
                track: track,
                raceDate: dateTime,
                participatingHorses: checked,
                bettedHorse: bettedHorse!
            }
            setIsFetching(true);
            RaceAPI.addNewRace(race).then(({data}) => {
                setRaceResults(data)
                setIsFetching(false);
            })
                .catch((err) => {
                    console.error(err);
                });
        }
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
    };

    const handleBack = () => {
        if (activeStep === 1) {
            setBettedHorse(undefined);
            setChecked([]);
        }
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        HorseAPI.getAllHorses()
            .then(({data}) => {
                setHorses(data)
                setIsFetching(false);
            })
            .catch((err) => {
                console.error(err);
            });
        setDateTime(new Date());
        setTrack('');
        setChecked([]);
        setBettedHorse(undefined);
        setRaceResults(undefined);
        setActiveStep(0);
    };

    const handleTrackChange = (event: SelectChangeEvent) => {
        setTrack(event.target.value);
    };


    useEffect(() => {
        HorseAPI.getAllHorses()
            .then(({data}) => {
                setHorses(data)
                setIsFetching(false);
            })
            .catch((err) => {
                console.error(err);
            });
    }, []);

    return (
        <Grid container spacing={2} justifyContent="center">
            <Grid item xs={12}>
                <Stepper activeStep={activeStep}>
                    {steps.map((label, index) => {
                        const stepProps: { completed?: boolean } = {};
                        const labelProps: {
                            optional?: React.ReactNode;
                        } = {};
                        return (
                            <Step key={label} {...stepProps}>
                                <StepLabel {...labelProps}>{label}</StepLabel>
                            </Step>
                        );
                    })}
                </Stepper>
            </Grid>
            <Grid item xs={12}>
                {activeStep === steps.length - 1 ? (
                    <Box>
                        <Typography variant="h4" align={"center"}>
                            Winner: {raceResults?.winningHorse?.name} (ID: {raceResults?.winningHorse?.id})
                        </Typography>
                        {raceResults?.winningHorse?.id === raceResults?.bettedHorse.id ? (
                            <Typography variant="body1" align={"center"}>
                                Congratulations you won!
                            </Typography>
                        ) : (
                            <Box mt={2}>
                                <Typography variant="body1" align={"center"}>
                                    You betted on: {raceResults?.bettedHorse?.name} (ID: {raceResults?.bettedHorse?.id})
                                </Typography>
                                <Typography variant="body1" align={"center"}>
                                    Better luck next time!
                                </Typography>
                            </Box>
                        )}

                        <Box sx={{display: 'flex', flexDirection: 'row', pt: 2}}>
                            <Box sx={{flex: '1 1 auto'}}/>
                            <Button onClick={handleReset}>Reset</Button>
                        </Box>
                    </Box>
                ) : (
                    <Box>
                        {activeStep === 0 ? (
                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <InputLabel id="track-label">Track</InputLabel>
                                        <Select
                                            required
                                            id="track"
                                            value={track}
                                            label="Race Track"
                                            onChange={handleTrackChange}
                                        >
                                            {raceTracks.map(track =>
                                                <MenuItem value={track.name}>{track.name}</MenuItem>
                                            )}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <LocalizationProvider dateAdapter={AdapterMoment}>
                                        <DateTimePicker
                                            label="Date&Time picker"
                                            disablePast
                                            value={dateTime}
                                            ampm={false}
                                            onChange={e => setDateTime(e!)}
                                            renderInput={(params) => <TextField {...params} fullWidth/>}
                                        />
                                    </LocalizationProvider>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="body1" align={"center"} m={2}>
                                        Choose participating horses:
                                    </Typography>
                                    {isFetching ? (<CircularProgress />) : (
                                        <DataGrid
                                            autoHeight
                                            rows={horses}
                                            columns={horseColumns}
                                            pageSize={20}
                                            rowsPerPageOptions={[20]}
                                            checkboxSelection
                                            onSelectionModelChange={(ids) => {
                                                const selectedIDs = new Set(ids);
                                                const selectedHorses = horses.filter((horse) =>
                                                    selectedIDs.has(horse.id!)
                                                )

                                                if (bettedHorse !== undefined) {
                                                    setBettedHorse(undefined)
                                                }
                                                setChecked(selectedHorses);
                                            }}
                                        />
                                    )}
                                </Grid>
                            </Grid>
                        ) : (<Box><Typography variant="body1" align={"center"} m={2}>
                                Select a horse to bet on:
                            </Typography>
                                <DataGrid
                                    autoHeight
                                    rows={checked}
                                    columns={horseColumns}
                                    pageSize={20}
                                    rowsPerPageOptions={[20]}
                                    onSelectionModelChange={(id) => {
                                        const selectedID = new Set(id);
                                        const selectedHorse = checked.filter((horse) =>
                                            selectedID.has(horse.id!)
                                        )
                                        setBettedHorse(selectedHorse[0]);
                                    }}
                                />
                            </Box>
                        )}
                        <Box sx={{display: 'flex', flexDirection: 'row', pt: 2}}>
                            <Button
                                color="inherit"
                                disabled={activeStep === 0 || activeStep === 2}
                                onClick={handleBack}
                                sx={{mr: 1}}
                            >
                                Back
                            </Button>
                            <Box sx={{flex: '1 1 auto'}}/>
                            <Button onClick={handleNext}
                                    disabled={(activeStep === 0 && (track === '' || checked.length < 2)) || (activeStep === 1 && bettedHorse === undefined)}>
                                {activeStep === steps.length - 2 ? 'Start race' : 'Next'}
                            </Button>
                        </Box>
                    </Box>
                )}
            </Grid>
        </Grid>
    )
}
