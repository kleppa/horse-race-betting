import React, {useEffect, useState} from 'react';
import {
    Alert,
    Button,
    CircularProgress,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField
} from "@mui/material";
import {Horse} from "../model/HorseModel";
import {HorseAPI} from "../api/HorseAPI";
import {DataGrid} from "@mui/x-data-grid";
import {horseColumns} from "../utils/DataGridColumnDef";


const horseColors = [
    {label: "Bay"},
    {label: "Chestnut"},
    {label: "Gray"},
    {label: "Black"},
    {label: "Roan"},
    {label: "Palomino"},
    {label: "Buckskin"},
    {label: "Dun"},
    {label: "Pinto"},
    {label: "Appaloosa"}
]


export const HorsesView = () => {
    const [horses, setHorses] = useState<Horse[]>([]);
    const [isFetching, setIsFetching] = useState(true);
    const [name, setName] = useState<string>('')
    const [color, setColor] = useState<string>('')
    const [weight, setWeight] = useState<number>()
    const [age, setAge] = useState<number>()
    const [quality, setQuality] = useState<number>()
    const [isInvalidInput, setIsInvalidInput] = useState(false);


    const handleAddHorse = () => {
        const horse: Horse = {
            name,
            color,
            weight,
            age,
            quality
        }
        if (weight !== null && (weight! < 60 || weight! > 90)) {
            setIsInvalidInput(true);
            setWeight(60);
        } else if (age !== null && (age! < 2 || age! > 15)) {
            setIsInvalidInput(true);
            setAge(2);
        } else if (quality !== null && (quality! < 1 || quality! > 10)) {
            setIsInvalidInput(true);
            setQuality(1);
        } else {
            setIsInvalidInput(false);
            HorseAPI.addNewHorse(horse).then(
                (data) => {
                    setIsFetching(true)
                    HorseAPI.getAllHorses()
                        .then(({data}) => {
                            setHorses(data)
                            setIsFetching(false);
                        })
                        .catch((err) => {
                            console.error(err);
                        })
                }
            )
                .catch((err) => {
                    console.error(err);
                });
        }

    }

    const handleColorChange = (event: SelectChangeEvent) => {
        setColor(event.target.value);
    };

    useEffect(() => {
        HorseAPI.getAllHorses()
            .then(({data}) => {
                setHorses(data)
                setIsFetching(false);
            })
            .catch((err) => {
                console.error(err);
            });
    }, []);

    return (
        <>
            <Grid container justifyContent="center" spacing={3} mt={2}>
                <Grid item xs={8} container justifyContent="center">
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item xs={6}>
                            <TextField
                                required
                                fullWidth
                                id="name"
                                label="Name"
                                onChange={(e) => setName(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <FormControl fullWidth>
                                <InputLabel id="select-label-color" required>Color</InputLabel>
                                <Select
                                    labelId="select-label-color"
                                    fullWidth
                                    id="color"
                                    value={color}
                                    label="Color"
                                    onChange={handleColorChange}
                                >
                                    {horseColors.map(color =>
                                        <MenuItem value={color.label}>{color.label}</MenuItem>
                                    )}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                id="age"
                                label="Age(years)"
                                type="number"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={age}
                                onChange={(e) => setAge(Number(e.target.value))}
                                InputProps={{inputProps: {min: 2, max: 15}}}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                id="weight"
                                label="Weight(kgs)"
                                type="number"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={weight}
                                onChange={(e) => setWeight(Number(e.target.value))}
                                InputProps={{inputProps: {min: 60, max: 90}}}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                id="quality"
                                label="Quality"
                                type="number"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={quality}
                                onChange={(e) => setQuality(Number(e.target.value))}
                                InputProps={{inputProps: {min: 1, max: 10}}}
                            />
                        </Grid>
                        <Grid item xs={6} container justifyContent="center">
                            <Button variant="contained" disabled={name === '' || color === ''} onClick={handleAddHorse}>Add
                                horse</Button>
                        </Grid>
                    </Grid>
                    {!isInvalidInput ? (
                            <></>
                        ) :
                        <Alert severity="error" style={{margin: 10}}>Invalid input!</Alert>
                    }
                </Grid>
                <Grid item xs={6} container justifyContent="center">
                    {!isFetching ? (
                            <DataGrid
                                autoHeight
                                rows={horses}
                                columns={horseColumns}
                                pageSize={20}
                                rowsPerPageOptions={[20]}
                            />
                        ) :
                        <CircularProgress/>}
                </Grid>
            </Grid>
        </>
    )

}
