import React from 'react';
import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {HorsesView} from "./horses/HorsesView";
import {HomePageView} from "./homepage/HomePageView";
import {RacesView} from "./races/RacesView";
import {Navbar} from "./navbar/Navbar";


function App() {
    return (
            <BrowserRouter>
                <Navbar/>
                <Routes>
                        <Route path="/" element={<HomePageView/>}/>
                        <Route path="/horses" element={<HorsesView/>}/>
                        <Route path="/races" element={<RacesView/>}/>
                </Routes>
            </BrowserRouter>
    );
}

export default App;
