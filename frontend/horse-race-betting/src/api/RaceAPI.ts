import axios, {AxiosResponse} from 'axios';
import {Config} from "../utils/Config";
import {Race} from "../model/RaceModel";

export class RaceAPI {
    static getAllRaces = async (): Promise<AxiosResponse<Race[]>> => {
        return axios.get(`${Config.apiURL}/race`, {
            headers: {
                Accept: 'application/json',
            },
        });
    };

    static addNewRace = async (
        race: Race
    ): Promise<AxiosResponse<Race>> => {
        return axios.post(`${Config.apiURL}/race`, race, {
            headers: {
                Accept: 'application/json',
            }
        })
    }
}
