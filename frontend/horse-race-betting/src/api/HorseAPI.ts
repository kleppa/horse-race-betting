import axios, {AxiosResponse} from 'axios';
import {Config} from "../utils/Config";
import {Horse} from "../model/HorseModel";

export class HorseAPI {
    static getAllHorses = async (): Promise<AxiosResponse<Horse[]>> => {
        return axios.get(`${Config.apiURL}/horse`, {
            headers: {
                Accept: 'application/json',
            },
        });
    };

    static addNewHorse = async (
        horse: Horse
    ): Promise<AxiosResponse<Horse>> => {
        return axios.post(`${Config.apiURL}/horse`, horse, {
            headers: {
                Accept: 'application/json',
            }
        })
    }
}
