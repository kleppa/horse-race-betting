import React, {useEffect, useState} from 'react';
import {Race} from "../model/RaceModel";
import {Grid} from "@mui/material";
import {RaceAPI} from "../api/RaceAPI";
import {DataGrid} from "@mui/x-data-grid";
import {raceColumns} from "../utils/DataGridColumnDef";

export const RacesView = () => {
    const [races, setRaces] = useState<Race[]>([]);


    useEffect(() => {
        RaceAPI.getAllRaces()
            .then(({data}) => {
                setRaces(data)
            })
            .catch((err) => {
                console.error(err);
            });
    }, []);


    return (
        <Grid container justifyContent="center" spacing={3} mt={2}>
            <Grid item xs={8} container justifyContent="center">
                <DataGrid
                    autoHeight
                    rows={races}
                    columns={raceColumns}
                    pageSize={20}
                    rowsPerPageOptions={[20]}
                />
            </Grid>
        </Grid>
    )
}
