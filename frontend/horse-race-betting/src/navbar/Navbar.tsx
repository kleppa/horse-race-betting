import {AppBar, Box, MenuItem, Toolbar} from "@mui/material";
import {Link} from "react-router-dom";


export const Navbar = () => {
    return (
        <Box sx={{flexGrow: 1}}>
            <AppBar position="static">
                <Toolbar>
                    <MenuItem component={Link} to={'/'}>Home</MenuItem>
                    <MenuItem component={Link} to={'/horses'}>Horses</MenuItem>
                    <MenuItem component={Link} to={'/races'}>Races</MenuItem>
                </Toolbar>
            </AppBar>
        </Box>
    )
}
