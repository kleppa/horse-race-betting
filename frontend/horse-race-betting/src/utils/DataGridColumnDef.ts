import {GridColDef} from "@mui/x-data-grid";
import {format} from "date-fns";

export const horseColumns: GridColDef[] = [
    {field: 'id', headerName: 'ID', minWidth: 70, flex: 1, align: "center", headerAlign: "center"},
    {field: 'name', headerName: 'Name', minWidth: 130, flex: 1, align: "center", headerAlign: "center"},
    {field: 'color', headerName: 'Color', minWidth: 130, flex: 1, align: "center", headerAlign: "center"},
    {
        field: 'weight',
        headerName: 'Weight (kgs)',
        type: 'number',
        minWidth: 90,
        flex: 1,
        align: "center",
        headerAlign: "center"
    },
    {
        field: 'age',
        headerName: 'Age (years)',
        type: 'number',
        minWidth: 90,
        flex: 1,
        align: "center",
        headerAlign: "center"
    },
    {
        field: 'quality',
        headerName: 'Quality',
        type: 'number',
        minWidth: 90,
        flex: 1,
        align: "center",
        headerAlign: "center"
    },
    {field: 'wins', headerName: 'Wins', type: 'number', minWidth: 90, flex: 1, align: "center", headerAlign: "center"},
];

export const raceColumns: GridColDef[] = [
    {field: 'id', headerName: 'ID', minWidth: 70, flex: 1, align: "center", headerAlign: "center"},
    {field: 'track', headerName: 'Track', minWidth: 130, flex: 1, align: "center", headerAlign: "center"},
    {
        field: 'date', headerName: 'Date', minWidth: 130, flex: 1, align: "center", headerAlign: "center",
        valueGetter: (params) => {
            return format(new Date(params.row.raceDate), 'dd/MM/yyyy HH:mm');
        }
    },
    {
        field: 'winningHorse',
        headerName: 'Winning Horse',
        minWidth: 90,
        flex: 1,
        align: "center",
        headerAlign: "center",
        valueGetter: (params) => {
            return params.row.winningHorse.name + " (ID: " + params.row.winningHorse.id.toString() + ")";
        }
    },
    {
        field: 'bettedHorse',
        headerName: 'Betted Horse',
        minWidth: 90,
        flex: 1,
        align: "center",
        headerAlign: "center",
        valueGetter: (params) => {
            return params.row.bettedHorse.name + " (ID: " + params.row.bettedHorse.id.toString() + ")";
        }
    },
];
