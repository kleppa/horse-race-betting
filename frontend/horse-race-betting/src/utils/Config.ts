const prod = {
        apiURL: "http://13.53.172.224:8080"
};
const dev = {
        apiURL: "http://localhost:8080"
};
export const Config = process.env.NODE_ENV === 'development' ? dev : prod;
